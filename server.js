const express = require('express')
const path = require('path')

const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'public'))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')

app.use('/', (req, res) => {
  res.render('index.html')
})

let messages = []

io.on('connection', socket => {
  console.log(`Socket connected: ${socket.id}`)

  socket.emit('previousMessage', messages)

  socket.on('sendMessage', data => {
    messages.push(data)
    socket.broadcast.emit('receivedMessage', data)
  })

  // o server emit quando um novo socket foi conectado
  socket.broadcast.emit('connectedSocket', { id: socket.id, message: `Socket connected: ${socket.id}` })

  socket.on('connectedSocket', data => { // quando um socket client conectar, repassa para os outros clients
    socket.broadcast.emit('connectedSocket', data)
  })
  socket.on("disconnect", () => { // caso algum socket se desconecte, é avisado para os clients
    socket.broadcast.emit('closetSocket', { id: socket.id, message: `Socket disconnected: ${socket.id}` })
  })
})

const PORT = 3000
server.listen(PORT, () => {
  console.log(`Server on at PORT ${PORT}`)
})